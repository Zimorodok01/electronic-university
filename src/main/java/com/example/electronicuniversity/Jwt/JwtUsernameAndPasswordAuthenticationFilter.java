package com.example.electronicuniversity.Jwt;

import com.example.electronicuniversity.Service.jwt.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class JwtUsernameAndPasswordAuthenticationFilter
        extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JwtConfig jwtConfig;
    private final TokenService tokenService;

    @Autowired
    public JwtUsernameAndPasswordAuthenticationFilter(
            AuthenticationManager authenticationManager,
            JwtConfig jwtConfig, TokenService tokenService) {
        this.authenticationManager = authenticationManager;
        this.jwtConfig = jwtConfig;
        this.tokenService = tokenService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {

        try {
            UsernameAndPasswordAuthenticationRequest authenticationRequest =
                    new ObjectMapper().readValue(request.getInputStream(),
                            UsernameAndPasswordAuthenticationRequest.class);
            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()
            );
            Authentication authenticate = authenticationManager.authenticate(authentication);
            return authenticate;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        //if user is not active, he cannot logged in
        boolean isActive = false;
        for (GrantedAuthority authority : authResult.getAuthorities()) {
            if (authority.getAuthority().equals("active"))
                isActive = true;
        }

        ResponseStatusException authenticationException = null;
        if (!isActive) {
            authenticationException =
                    new ResponseStatusException(BAD_REQUEST, "You are not active");
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getOutputStream()
                    .println("{ \"error\": \"" + authenticationException.getMessage() + "\" }");
        }



        String refreshToken = tokenService.getRefreshToken(authResult.getName(),
                authResult.getAuthorities(),
                jwtConfig.getTokenExpirationAfterDays());
        response.addHeader("Refresh token",
                refreshToken);

        String accessToken = tokenService.getAccessToken(authResult.getName(),
                authResult.getAuthorities());
        response.addHeader("Access token", accessToken);

    }
}
