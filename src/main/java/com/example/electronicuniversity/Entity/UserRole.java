package com.example.electronicuniversity.Entity;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.example.electronicuniversity.Entity.UserPermission.*;

@AllArgsConstructor
@Getter
public enum UserRole {
    STUDENT(Sets.newHashSet(
            COURSE_READ,
            STUDENT_STATUS_READ
    )),
    TEACHER(Sets.newHashSet(
            STUDENT_READ,
            STUDENT_STATUS_READ,
            STUDENT_STATUS_WRITE
    )),
    RECTOR(Sets.newHashSet(
            COURSE_READ,
            COURSE_WRITE,
            STUDENT_READ,
            STUDENT_WRITE,
            TEACHER_READ,
            TEACHER_WRITE,
            STUDENT_STATUS_READ,
            STUDENT_STATUS_WRITE
    ));

    private Set<UserPermission> permissions;

    public Set<SimpleGrantedAuthority> getGrantedAuthority() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
