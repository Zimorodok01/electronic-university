package com.example.electronicuniversity.Service.jwt;

import com.example.electronicuniversity.Jwt.JwtConfig;
import com.example.electronicuniversity.Repository.UserRepository;
import com.google.common.base.Strings;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.crypto.SecretKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class TokenServiceImpl implements TokenService {
    private final SecretKey secretKey;
    private final UserRepository userRepository;
    private final JwtConfig jwtConfig;

    @Autowired
    public TokenServiceImpl(SecretKey secretKey, UserRepository userRepository, JwtConfig jwtConfig) {
        this.secretKey = secretKey;
        this.userRepository = userRepository;
        this.jwtConfig = jwtConfig;
    }

    public Map<String, String> refreshToken(String refreshToken) {
        if (Strings.isNullOrEmpty(refreshToken) ||
                !refreshToken.startsWith("Bearer ")) {
            throw new ResponseStatusException(BAD_REQUEST,"Token is invalid");
        }

        refreshToken = refreshToken.replace("Bearer ","");

        String subject;
        Set<SimpleGrantedAuthority> simpleGrantedAuthorities;

        try {
            Jws<Claims> jws = Jwts.parser().setSigningKey(secretKey)
                    .parseClaimsJws(refreshToken);

            Claims body = jws.getBody();

            subject = body.getSubject();

            if (!userRepository.findByEmail(subject).isPresent()) {
                throw new ResponseStatusException(BAD_REQUEST,String.format(
                        "User with email %s doesn't register",subject
                ));
            }

            String claim = (String) body.get("token");
            if (!claim.equals("REFRESH")) {
                throw new ResponseStatusException(BAD_REQUEST,"This is not a refresh token");
            }

            List<Map<String, String>> authorities =
                    (List<Map<String,String>> ) body.get("authorities");

            simpleGrantedAuthorities = authorities.stream()
                    .map(m -> new SimpleGrantedAuthority(m.get("authority")))
                    .collect(Collectors.toSet());

            Date expiredDate = body.getExpiration();
            if (expiredDate.before(new Date())) {
                throw new ResponseStatusException(BAD_REQUEST,"Your token is expired");
            }
        } catch (JwtException e) {
            throw new ResponseStatusException(BAD_REQUEST,String.format("Token %s cannot be trusted",refreshToken));
        }

        Map<String,String> tokens = new HashMap<>();

        tokens.put("Access Token",
                getAccessToken(subject,simpleGrantedAuthorities));
        tokens.put("Refresh Token",
                getRefreshToken(subject,simpleGrantedAuthorities,
                        jwtConfig.getTokenExpirationAfterDays()));

        return tokens;
    }

    @Override
    public String getUserByToken(String token) {
        if (Strings.isNullOrEmpty(token) ||
                !token.startsWith("Bearer ")) {
            throw new IllegalStateException("Token is invalid");
        }

        token = token.replace("Bearer ","");

        try {
            Jws<Claims> jws = Jwts.parser().setSigningKey(secretKey)
                    .parseClaimsJws(token);

            Claims body = jws.getBody();

            String subject = body.getSubject();

            String tokenType = (String) body.get("token");

            if (!tokenType.equals("ACCESS")) {
                throw new ResponseStatusException(BAD_REQUEST,"Token is not for access");
            }

            return subject;
        } catch (JwtException e) {
            throw new IllegalStateException(String.format("Token %s cannot be trusted",token));
        }
    }

    @Override
    public String getAccessToken(String subject, Collection<? extends GrantedAuthority> authorities) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY,1);
        return "Bearer " + Jwts.builder()
                .setSubject(subject)
                .claim("authorities", authorities)
                .claim("token","ACCESS")
                .setIssuedAt(new Date())
                .setExpiration(calendar.getTime())
                .signWith(secretKey)
                .compact();
    }

    @Override
    public String getRefreshToken(String subject, Collection<? extends GrantedAuthority> authorities,
                                         Integer days) {
        return "Bearer " + Jwts.builder()
                .setSubject(subject)
                .claim("authorities", authorities)
                .claim("token","REFRESH")
                .setIssuedAt(new Date())
                .setExpiration(java.sql.Date.valueOf(LocalDate.now()
                        .plusDays(days)))
                .signWith(secretKey)
                .compact();
    }
}
