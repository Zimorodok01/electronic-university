package com.example.electronicuniversity.Entity;

import com.example.electronicuniversity.Entity.dto.UserDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

import static com.example.electronicuniversity.Entity.UserPermission.*;

@Getter
@Entity
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue
    private Long userId;
    @Setter
    private String name;
    @Setter
    private String password;
    @Setter
    private String email;
    @Enumerated(EnumType.STRING)
    @Setter
    private UserRole role;
    private Boolean isActive = false;
    private Boolean isAccountNonExpired = true;
    private Boolean isAccountNonLocked = true;
    private Boolean isCredentialsNonExpired = true;
    private Boolean isEnabled = true;

    public User(String name, String password,
                String email, UserRole role,
                boolean isAccountNonExpired,
                boolean isAccountNonLocked,
                boolean isCredentialsNonExpired, boolean isEnabled) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.role = role;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
    }

    public User() {

    }

    public User(UserDto userDto) {
        this.name = userDto.getName();
        this.email = userDto.getEmail();
        this.password = userDto.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (isActive) {
            Set<SimpleGrantedAuthority> grantedAuthorities = role.getGrantedAuthority();
            grantedAuthorities.add(new SimpleGrantedAuthority(ACTIVE.getPermission()));
            return grantedAuthorities;
        }
        return role.getGrantedAuthority();
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setAccountNonExpired(Boolean isAccountNonExpired) {
        this.isAccountNonExpired = isAccountNonExpired;
    }
    public void setAccountNonLocked(Boolean isAccountNonLocked) {
        this.isAccountNonLocked = isAccountNonLocked;
    }
    public void setCredentialsNonExpired(
            Boolean isCredentialsNonExpired) {
        this.isCredentialsNonExpired = isCredentialsNonExpired;
    }
    public void setEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }


}
