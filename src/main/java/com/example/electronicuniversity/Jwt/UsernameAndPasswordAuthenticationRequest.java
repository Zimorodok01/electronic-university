package com.example.electronicuniversity.Jwt;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
public class UsernameAndPasswordAuthenticationRequest {

    private String username;
    private String password;


}
