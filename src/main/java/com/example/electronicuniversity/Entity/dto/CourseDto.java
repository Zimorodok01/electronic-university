package com.example.electronicuniversity.Entity.dto;

import com.example.electronicuniversity.Entity.User;
import lombok.Getter;

@Getter
public class CourseDto {
    private String name;
    private Integer price;
    private Integer teacher;
}
