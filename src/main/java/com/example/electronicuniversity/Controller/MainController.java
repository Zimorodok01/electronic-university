package com.example.electronicuniversity.Controller;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.UserDto;
import com.example.electronicuniversity.Service.UserServiceImpl;
import com.example.electronicuniversity.Service.jwt.TokenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping("/api/v1")
public class MainController {
    private final UserServiceImpl userService;
    private final TokenServiceImpl tokenService;

    @Autowired
    public MainController(UserServiceImpl userService, TokenServiceImpl tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @PostMapping(path = "/registration")
    public String registration(@RequestBody UserDto user) {
        return userService.registerNewUser(user);
    }

    @PostMapping(path = "/registration/confirm")
    public Map<String, String> confirmRegistration(
            @RequestParam String email,
            @RequestParam int code) {
        return userService.checkCode(email,code);
    }

    @PutMapping(path = "/refresh")
    public Map<String, String> checkRefreshToken
            (@RequestHeader(value = "Authorization") String authorization) {
        return tokenService.refreshToken(authorization);
    }
}
