package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.StudentStatus;
import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.StudentStatusDto;

import java.util.List;

public interface StudentStatusService {
    List<StudentStatusDto> getStudentStatuses(String token);

    StudentStatusDto getStudentStatus(String token, long studentStatusId);

    StudentStatus deleteStudentStatus(String token, long studentStatusId);

    void signStudent(User student, Course course);

    void updateStudentStatus(long studentStatusId, String token, Integer grade, Double attendance);



}
