package com.example.electronicuniversity.Controller;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.dto.CourseDto;
import com.example.electronicuniversity.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/courses")
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PreAuthorize("hasAuthority('course:read')")
    @GetMapping
    public List<Course> getCourses(
            @RequestHeader(value = "Authorization") String token) {
        return courseService.getCourses(token);
    }

    @PreAuthorize("hasAuthority('course:read')")
    @GetMapping(path = "{course_id}")
    public Course getCourse(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "course_id") Long courseId) {
        return courseService.getCourse(token,courseId);
    }

    @PreAuthorize("hasRole('RECTOR')")
    @PostMapping
    public Course addCourse(
            @RequestHeader(value = "Authorization") String token,
            @RequestBody CourseDto courseDto) {
        return courseService.addCourse(token, courseDto);
    }

    @PreAuthorize("hasRole('RECTOR')")
    @PutMapping("/{courseId}")
    public Course updateCourse(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "courseId") Long courseId,
            @RequestParam Long teacherId) {
        return courseService.updateCourse(token,courseId,teacherId);
    }


    @PreAuthorize("hasRole('RECTOR')")
    @DeleteMapping(path = "{course_id}")
    public Course deleteCourse(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "course_id") Long courseId) {
        return courseService.deleteCourse(token,courseId);
    }

    @PreAuthorize("hasRole('STUDENT')")
    @PostMapping("{courseId}/signIn")
    public String signInToCourse(@RequestHeader(value = "Authorization") String token,
                               @PathVariable("courseId") Long courseId) {
        return courseService.signIn(token, courseId);
    }
}
