package com.example.electronicuniversity.Repository;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CourseRepository extends JpaRepository<Course,Long> {
    Course findByCourseId(Long id);

    List<Course> findByName(String name);

    List<Course> findAllByTeacher(User teacher);
}
