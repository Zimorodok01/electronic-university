package com.example.electronicuniversity.Repository;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserId(Long id);

    List<User> findAllByRole(UserRole role);

    Optional<User> findByEmail(String email);
}
