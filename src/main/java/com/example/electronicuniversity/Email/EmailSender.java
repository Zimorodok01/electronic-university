package com.example.electronicuniversity.Email;

public interface EmailSender {
    void sendMail(String to, String email);
}
