package com.example.electronicuniversity.Entity;

import com.example.electronicuniversity.Entity.dto.StudentStatusDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
public class StudentStatus {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JsonBackReference
    private Course course;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User student;
    @Setter
    private Integer grade;
    @Setter
    private Double attendance;

    public StudentStatus(Course course, User student, Integer grade, Double attendance) {
        this.course = course;
        this.student = student;
        this.grade = grade;
        this.attendance = attendance;
    }

    public StudentStatusDto getDto() {
        StudentStatusDto dto = new StudentStatusDto(
                id,course.getName(),student.getName(),grade,attendance
        );

        return dto;
    }
}
