package com.example.electronicuniversity.Email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {
    @Value("${mail.username}")
    private String email;
    @Value("${mail.password}")
    private String password;
    @Value("${mail.host}")
    private String host;
    @Value("${mail.smtps.auth}")
    private String mailSmtpAuth;
    @Value("${mail.protocol}")
    private String protocol;


    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", mailSmtpAuth);
        javaMailSender.setJavaMailProperties(properties);
        javaMailSender.setHost(host);
        javaMailSender.setPort(465);
        javaMailSender.setUsername(email);
        javaMailSender.setPassword(password);
        javaMailSender.setProtocol(protocol);
        return javaMailSender;
    }
}
