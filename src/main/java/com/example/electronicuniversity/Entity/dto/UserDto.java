package com.example.electronicuniversity.Entity.dto;

import com.example.electronicuniversity.Entity.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
public class UserDto {
    private String name;
    private String password;
    private String email;
}
