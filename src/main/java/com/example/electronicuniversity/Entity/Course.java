package com.example.electronicuniversity.Entity;

import com.example.electronicuniversity.Entity.dto.CourseDto;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue
    private Long courseId;
    @Setter
    private String name;
    @Setter
    private Integer price;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "teacher_id")
    @Setter
    private User teacher;

    public Course(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public Course() {
    }
}
