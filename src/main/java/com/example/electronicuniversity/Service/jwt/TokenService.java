package com.example.electronicuniversity.Service.jwt;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;

public interface TokenService {
    Map<String, String> refreshToken(String refreshToken);

    String getUserByToken(String token);

    String getAccessToken(String subject, Collection<? extends GrantedAuthority> authorities);

    String getRefreshToken(String subject, Collection<? extends GrantedAuthority> authorities,
                           Integer days);
}
