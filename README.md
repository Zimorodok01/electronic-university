# Electronic University
### Database Structure
####  Tables
* Users
  * Students
  * Teachers
  * Rectors
* Courses
* Student Statuses

Between Students and Courses have Many to many connection
Students M-----------------------M Courses. And Student 
status table is relationship this entities

![diagram_structure](https://user-images.githubusercontent.com/89076404/129716042-7d9f8393-85a6-46fa-b135-d2f9ffd53a78.jpg)


    And i use Redis  NoSQL database for storing code that 
    user could receive from mail

Students can choose their courses, but limit of courses is 3. 
Teachers can put grades and attendance.
Rector can add or delete students and teachers, courses

### Roles and their permissions
  * STUDENT
    * COURSE_READ
    * STUDENT_STATUS_READ
  * TEACHER
    * STUDENT_READ
    * STUDENT_STATUS_READ
    * STUDENT_STATUS_WRITE
  * RECTOR
    * COURSE_READ
    * COURSE_WRITE
    * STUDENT_READ
    * STUDENT_WRITE
    * TEACHER_READ
    * TEACHER_WRITE
    * STUDENT_STATUS_READ
    * STUDENT_STATUS_WRITE

### Permissions
####  STUDENT_READ
     getStudents()
        Route: "api/students"
        Method: GET
        Get all students from database
     getStudent(studentId Int)
        Route: "api/students/{studentId}"
        Method: GET
        Get student from database by id
####  STUDENT_WRITE
     addStudent(student User)
        Route: "api/students"
        Method: POST
        Add a new student. If password is null, 
        we set default password 
     deleteStudent(studentId Int)
        Route: "api/students/{studentId}"
        Method: DELETE
        Remove student from database
####  COURSE_READ
     getCourses()
        Route: "api/courses"
        Method: GET
        Get all courses from database
     getcourse(courseId Int)
        Route: "api/courses/{courseId}"
        Method: GET
        Get course from database by id  
#### COURSE_WRITE
     addCourse(course Course)
        Route: "api/courses"
        Method: POST
        Add a new course
     deleteCourse(courseId Int)
        Route: "api/courses/{courseId}"
        Method: DELETE
        Delete course from database  
#### TEACHER_READ
     getTeachers()
        Route: "api/teachers"
        Method: GET
        Get all teachers from database
     getTeacher(teacherId Int)
        Route: "api/teachers/{teacherId}"
        Method: GET
        Get course from database by id 
#### TEACHER_WRITE
     addTeacher(teacher Teacher)
        Route: "api/teachers"
        Method: POST
        Add a new teacher. If password is null, 
        we set default password
     deleteTeacher(teacherId Int)
        Route: "api/teachers/{teacherId}"
        Method: DELETE
        Delete teacher from database 
#### STUDENT_STATUS_READ
    getStudentStatuses()
        Route: "api/students/status"
        Method: GET
        Get all teachers from database
     getStudentStatus(studentStatusId Int)
        Route: "api/students/status/{studentStatusId}"
        Method: GET
        Get course from database by id 
#### STUDENT_STATUS_WRITE
    updateStudentStatus(studentStatusId Int,grade Int
                        attendance Double)
        Route: "api/students/status/{studentStatusId}"
        Method: PUT
        Teacher can put grades and attendance. Methods
        required only TEACHER role
     delStudentStatus(studentStatusId Int)
        Route: "api/students/status/{studentStatusId}"
        Method: DELETE
        Delete student status
#### Methods without permissions
     signInToCourse(courseId Int)
        Route: "api/courses/{courseId}/signIn"
        Method: POST
        Sign in to course. Only for students!!!
     registration(user User)
        Route: "/registration"
        Method: POST
        Register new student and send code to his mail
     confirmRegistration(email String, code Int)
        Route: "/registration/confirm"
        Method: POST
        After receiving mail message, user should confirm his
        registration and get token and be active
     checkRefreshToken()
        Route: "/refresh"
        Method: PUT
        After expiring access token, user can get new two 
        token by using refresh token that located in header
        section