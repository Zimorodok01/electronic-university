package com.example.electronicuniversity.Repository;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.StudentStatus;
import com.example.electronicuniversity.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface StudentStatusRepository extends JpaRepository<StudentStatus,Long> {
    StudentStatus findById(long id);

    List<StudentStatus> findAllByStudent(User student);

    Optional<StudentStatus> findByCourseAndStudent(Course course,User student);
}
