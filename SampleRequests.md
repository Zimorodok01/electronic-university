# Examples

### Registration
Route:api/v1/registration
Method: POST

    Request body
    {
        "name" : "Ramazan Baigazy",
        "email" : "ramazanbaigazi@gmail.com",
        "password" : "password"
    }
### Confirm registration
Route: api/v1/registration/confirm
Method: POST

    Request Parameter 
    email: ramazanbaigazi@gmail.com
    code: 7749 (for example, this code can be invalid)
### Login
Route: /login
Method: POST

    Request body
    {
        "username" : "admin@gmail.com",
        "password" : "password"
    }
### Refreshing token
Route: api/v1/refresh
Method: PUT

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
### Get course list
Required permission: COURSE_READ
Route: api/v1/courses
Method: GET
    
    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
### Get course
Required permission: COURSE_READ
Route: api/v1/courses/{courseId}
Method: GET
    
    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter courseId
### Add course
Required role: RECTOR
Route: api/v1/courses
Method: POST

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Request body
    {
        "name" : "Java Security",
        "price" : "1000",
        "teacher" : "2"
    }
### Update Course
Required role: RECTOR
Route: api/v1/courses/{courseId}
METHOD: PUT

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter courseId
    Request Parameter 
    teacherId: 48
### Delete course
Required role: RECTOR
Route: api/v1/courses/{courseId}
Method: DELETE

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter courseId
### Sign in to course
Required role: STUDENT
Route: api/v1/courses/{courseId}/signIn
Method: POST

    Request header
    key = Authorization, value = token (it should be 
                                        access token)
    Parameter courseId
### Get student list
Required permission: STUDENT_READ
Route: api/v1/students
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
### Get student
Required permission: STUDENT_READ
Route: api/v1/students/{studentId}
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter studentId
### Add student
Required role: RECTOR
Route: api/v1/students
Method: POST

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Request body
    {
        "name" : "Ramazan",
        "password" : "qwerty" (by default "password"),
        "email" : "ramazanbaigazy1312@gmail.com"
    }
### Delete student
Required role: RECTOR
Route: api/v1/students/{studentId}
Method: DELETE

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter studentId
### Get all students status list
Required roles: TEACHER and RECTOR
Route: api/v1/students/status
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
### Get student's status
Required permission: ACTIVE
Route: api/v1/students/status/{studentStatusId}
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter studentStatusId
### Update student status
Required role: TEACHER
Route: api/v1/students/status/{studentStatusId}
Method: PUT

    Parameter studentStatusId

    Request header
    key = Authorization, value = token (it should be 
                                        access token)

    Request Parameter 
    grade: 100
    attendance : 100
### Delete student status
Required role: RECTOR
Route: api/v1/students/status/{studentStatusId}
Method: DELETE

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter studentStatusId
### Get teacher list
Required role: RECTOR
Route: api/v1/teachers
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
### Get teacher
Required role: RECTOR
Route: api/v1/teachers/{teacherId}
Method: GET

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter teacherId
### Add teacher
Required role: RECTOR
Route: api/v1/teachers
Method: POST

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
     Request body
    {
        "name" : "Ramazan",
        "password" : "qwerty" (by default "password"),
        "email" : "ramazanbaigazy1312@gmail.com"
    }
### Delete teacher
Required role: RECTOR
Route: api/v1/teachers/{teacherId}
Method: DELETE

    Request header
    key = Authorization, value = token (it should be 
                                        refresh token)
    Parameter teacherId