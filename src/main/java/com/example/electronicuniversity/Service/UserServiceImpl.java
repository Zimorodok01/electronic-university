package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Email.EmailSender;
import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.UserRole;
import com.example.electronicuniversity.Entity.dto.UserDto;
import com.example.electronicuniversity.Jwt.JwtConfig;
import com.example.electronicuniversity.Repository.UserRepository;
import com.example.electronicuniversity.Service.jwt.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.electronicuniversity.Entity.UserRole.STUDENT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailSender emailSender;
    private final JwtConfig jwtConfig;
    private final TokenService tokenService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailSender emailSender, JwtConfig jwtConfig, TokenService tokenService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSender = emailSender;
        this.jwtConfig = jwtConfig;
        this.tokenService = tokenService;
    }

    public List<User> getUsers(String token, UserRole role) {
        User user = userRepository.findByEmail(
                tokenService.getUserByToken(token)).orElseThrow(() ->
                new ResponseStatusException(BAD_REQUEST, "User doesn't exists")
        );
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,
                    "You doesn't have access");
        }
        return userRepository.findAllByRole(role);
    }

    public User getUser(String token, Long userId) {
        User auth = userRepository.findByEmail(
                tokenService.getUserByToken(token)).orElseThrow(() ->
                new ResponseStatusException(BAD_REQUEST, "User doesn't exists")
        );
        if (!auth.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,
                    "You doesn't have access");
        }

        return getUser(userId);
    }

    @Override
    public User getUser(Long userId) {
        User user = userRepository.findByUserId(userId);
        if (user == null)
            throw new ResponseStatusException(BAD_REQUEST,"User with id " + userId
                    + " doesn't exist");

        return user;
    }

    public User addUser(String token, User user) {
        User auth = userRepository.findByEmail(
                tokenService.getUserByToken(token)).orElseThrow(() ->
                new ResponseStatusException(BAD_REQUEST, "User doesn't exists")
        );
        if (!auth.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,
                    "You doesn't have access");
        }

        if (user.getName() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "User name is null");
        }

        if (user.getEmail() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Email is required");
        }

        if (user.getPassword() == null) {
           throw new ResponseStatusException(BAD_REQUEST,"Password is required");
        } else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        user.setActive(true);

        return userRepository.save(user);
    }

    public User deleteUser(User user) {
        userRepository.delete(user);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format(
                                "Username with email %s not found",
                                email)));
    }

    public String registerNewUser(UserDto userDto) {

        if (userDto.getName() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "User's name is null");
        }

        if (userDto.getEmail() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Email is required");
        }

        if (userDto.getPassword() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Password cannot be null");
        }

        User user = new User(userDto);
        user.setRole(STUDENT);


        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(BAD_REQUEST,
                    String.format("Email %s is already taken", user.getEmail()));
        }

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        String password = user.getPassword();
        user.setPassword(passwordEncoder.encode(password));
        user.setActive(false);

        String email = user.getEmail();
        Pattern pattern = Pattern.compile("^[A-Za-z0-9+_.]+@(.+)$");
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            throw new ResponseStatusException(BAD_REQUEST, "Email is envalid");
        }


        int codeValidation = generateCode();
        Jedis jedis = new Jedis();
        jedis.set(user.getEmail(), String.valueOf(codeValidation));

        emailSender.sendMail(user.getEmail(),
                buildEmail(user.getName(), codeValidation));
        userRepository.save(user);
        return "Enter code for confirmation";
    }

    private String buildEmail(String name, int code) {
        return "<div style=\"font-family: Helvetica,Arial,sans-serif;font-size: 16px;margin:0;color:#0b0c0c\">\n"
                + name + "\n" +
                "This is your code validation. Don't lost it\n"
                + code + "\n</div>";
    }

    private Integer generateCode() {
        int random = (int) (Math.random() * 9999);
        if (random <= 1000)
            return generateCode();
        return random;
    }

    public Map<String, String> checkCode(String email, int userCode) {
        User user = userRepository.findByEmail(email)
                .orElseThrow((() ->
                        new UsernameNotFoundException("Username not found")));
        Jedis jedis = new Jedis();
        String code = jedis.get(email);

        if (!code.equals(String.valueOf(userCode)))
            throw new ResponseStatusException(BAD_REQUEST, "Your code is incorrect");

        user.setActive(true);
        userRepository.save(user);
        jedis.del(email);

        Map<String, String> response = new HashMap<>();

        String accessToken = tokenService.getAccessToken(user.getEmail(), user.getAuthorities());
        String refreshToken = tokenService.getRefreshToken(user.getEmail(), user.getAuthorities(),
                jwtConfig.getTokenExpirationAfterDays());

        response.put("Access Token", accessToken);
        response.put("Refresh Token", refreshToken);

        return response;


    }


    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(
                        () -> new ResponseStatusException(BAD_REQUEST, String.format("User with %s not found", email))
                );
    }
}
