package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.StudentStatus;
import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.StudentStatusDto;
import com.example.electronicuniversity.Repository.StudentStatusRepository;
import com.example.electronicuniversity.Service.jwt.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.example.electronicuniversity.Entity.UserRole.STUDENT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@RequiredArgsConstructor
public class StudentStatusServiceImpl implements StudentStatusService {
    private final StudentStatusRepository studentStatusRepository;
    private final TokenService tokenService;
    private final UserService userService;


    public List<StudentStatusDto> getStudentStatuses(String token) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token)
        );
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST, "You don't have access");
        }
        List<StudentStatusDto> studentStatuses = new ArrayList<>();
        studentStatusRepository.findAll().forEach(
                status -> studentStatuses.add(status.getDto())
        );

        return studentStatuses;
    }

    public StudentStatusDto getStudentStatus(String token, long studentStatusId) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token)
        );
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST, "You don't have access");
        }

        if (user.getUserId() != studentStatusId && user.getRole() == STUDENT) {
            throw new ResponseStatusException(BAD_REQUEST, "This isn't your status");
        }
        StudentStatus status = studentStatusRepository.findById(studentStatusId);

        if (status == null)
            throw new ResponseStatusException(BAD_REQUEST,
                    "You enter wrong student status id");

        return status.getDto();
    }

    public StudentStatus deleteStudentStatus(String token, long studentStatusId) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token)
        );
        if (user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST, "You don't have access");
        }

        StudentStatus studentStatus = studentStatusRepository.findById(studentStatusId);
        studentStatusRepository.delete(studentStatus);
        return studentStatus;
    }

    public void signStudent(User student, Course course) {
        if (studentStatusRepository.findByCourseAndStudent(course, student).isPresent()) {
            throw new ResponseStatusException(BAD_REQUEST,"You already signed in");
        }


        List<StudentStatus> studentStatuses =
                studentStatusRepository.findAllByStudent(student);

        if (studentStatuses.size() == 3) {
            throw new ResponseStatusException(BAD_REQUEST, "You have enough courses");
        }

        StudentStatus studentStatus = new StudentStatus(
                course, student, null, null
        );

        studentStatusRepository.save(studentStatus);
    }

    public void updateStudentStatus(long studentStatusId, String token, Integer grade, Double attendance) {

        if (grade < 0 || grade > 100) {
            throw new ResponseStatusException(BAD_REQUEST,"Grade should be between 0 and 100");
        }

        if (attendance < 0.0 || attendance > 100.0) {
            throw new ResponseStatusException(BAD_REQUEST,"Attendance should be between 0.0 and 100.0");
        }

        User teacher = userService.getUserByEmail(
                tokenService.getUserByToken(token)
        );

        if (!teacher.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST, "You don't have access");
        }
        StudentStatus studentStatus = studentStatusRepository.findById(studentStatusId);

        if (studentStatus == null) {
            throw new ResponseStatusException(BAD_REQUEST,"Wrong student status id");
        }

        if (studentStatus.getCourse().getTeacher().getUserId() != teacher.getUserId()) {
            throw new ResponseStatusException(BAD_REQUEST, "This isn't your course");
        }

        if (grade != null) {
            studentStatus.setGrade(grade);
        }

        if (attendance != null) {
            studentStatus.setAttendance(attendance);
        }
        studentStatusRepository.save(studentStatus);
    }

}
