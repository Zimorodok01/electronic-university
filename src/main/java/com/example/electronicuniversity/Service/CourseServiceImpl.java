package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.CourseDto;
import com.example.electronicuniversity.Repository.CourseRepository;
import com.example.electronicuniversity.Service.jwt.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.example.electronicuniversity.Entity.UserRole.TEACHER;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class CourseServiceImpl implements CourseService{
    private final CourseRepository courseRepository;
    private final StudentStatusServiceImpl studentStatusService;
    private final TokenService tokenService;
    private final UserService userService;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, StudentStatusServiceImpl studentStatusService, TokenService tokenService, UserService userService) {
        this.courseRepository = courseRepository;
        this.studentStatusService = studentStatusService;
        this.tokenService = tokenService;
        this.userService = userService;
    }

    public List<Course> getCourses(String token) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token));
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }

        return courseRepository.findAll();
    }

    @Override
    public Course updateCourse(String token, Long courseId, Long teacherId) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token));
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }

        Course course = courseRepository.findByCourseId(courseId);
        if (course == null) {
            throw new ResponseStatusException(BAD_REQUEST,"Course doesn't exist");
        }

        User newTeacher = userService.getUser(teacherId);
        if (newTeacher.getRole() != TEACHER) {
            throw new ResponseStatusException(BAD_REQUEST,
                    "Course's teacher is not identified");
        }

        course.setTeacher(newTeacher);

        return courseRepository.save(course);

    }

    @Override
    public boolean hasTeacher(User teacher) {
        return !courseRepository.findAllByTeacher(teacher).isEmpty();
    }

    public Course getCourse(String token, Long courseId) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token));
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }

        return courseRepository.findById(courseId).orElseThrow(
                ()->new ResponseStatusException(BAD_REQUEST , "course " +
                    "does not exist"));
    }

    public Course addCourse(String token,CourseDto courseDto) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token));
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }


        Course course = new Course(courseDto.getName(), courseDto.getPrice());

        if (courseRepository.findByName(course.getName()).size() > 0) {
            throw new ResponseStatusException(BAD_REQUEST,"Course with name "
            + course.getName() + " exists");
        }

        User teacher = userService.getUser(token, Long.valueOf(courseDto.getTeacher()));

        if (teacher.getRole() != TEACHER) {
            throw new ResponseStatusException(BAD_REQUEST,"Teacher with id " +
                    courseDto.getTeacher() + " doesn't exist");
        }
        course.setTeacher(teacher);


        return courseRepository.save(course);
    }

    public Course deleteCourse(String token,Long courseId) {
        User user = userService.getUserByEmail(
                tokenService.getUserByToken(token));
        if (!user.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }

        Course course = courseRepository.findByCourseId(courseId);
        if (course == null) {
            throw new ResponseStatusException(BAD_REQUEST,"Course with id " + courseId
            + " doesn't exist");
        }
        courseRepository.delete(course);
        return course;
    }

    public String signIn(String token, Long courseId) {
        User student = userService.getUserByEmail(
                tokenService.getUserByToken(token)
        );
        if (!student.getIsActive()) {
            throw new ResponseStatusException(BAD_REQUEST,"You don't have access");
        }

        Course course = courseRepository.findByCourseId(courseId);

        studentStatusService.signStudent(student,course);

        return "You succesfully signed in";
    }
}
