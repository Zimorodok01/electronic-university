package com.example.electronicuniversity.Controller;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.UserDto;
import com.example.electronicuniversity.Service.CourseService;
import com.example.electronicuniversity.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.example.electronicuniversity.Entity.UserRole.TEACHER;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping(path = "api/v1/teachers")
public class TeacherController {
    private final UserService userService;
    private final CourseService courseService;

    @Autowired
    public TeacherController(UserService userService, CourseService courseService) {
        this.userService = userService;
        this.courseService = courseService;
    }


    @PreAuthorize("hasAuthority('teacher:read')")
    @GetMapping
    public List<User> getTeachers(
            @RequestHeader(value = "Authorization") String token
    ) {
        return userService.getUsers(token,TEACHER);
    }

    @PreAuthorize("hasAuthority('teacher:read')")
    @GetMapping(path = "{teacher_id}")
    public User getTeacher(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "teacher_id")Long teacherId) {
        return userService.getUser(token,teacherId);
    }

    @PreAuthorize("hasAuthority('teacher:write')")
    @PostMapping
    public User addTeacher(
            @RequestHeader(value = "Authorization") String token,
            @RequestBody UserDto userDto) {
        User user = new User(userDto);
        user.setRole(TEACHER);

        return userService.addUser(token,user);
    }

    @PreAuthorize("hasAuthority('teacher:write')")
    @DeleteMapping(path = "{teacher_id}")
    public User deleteTeacher(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "teacher_id") Long teacherId) {

        User teacher = userService.getUser(token, teacherId);

        if (teacher.getRole() != TEACHER) {
            throw new ResponseStatusException(BAD_REQUEST,"This id is not teacherId");
        }

        if (courseService.hasTeacher(teacher)) {
            throw new ResponseStatusException(BAD_REQUEST,"This teacher has course or courses." +
                    " If you want delete him delete course or change teacher to course");
        }


        return userService.deleteUser(teacher);
    }



}
