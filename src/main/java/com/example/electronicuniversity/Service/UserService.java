package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.UserRole;
import com.example.electronicuniversity.Entity.dto.UserDto;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getUsers(String token, UserRole role);

    User getUser(String token, Long userId);

    User addUser(String token, User user);

    User deleteUser(User user);

    String registerNewUser(UserDto userDto);

    Map<String, String> checkCode(String email, int userCode);

    User getUserByEmail(String email);

    User getUser(Long teacherId);
}
