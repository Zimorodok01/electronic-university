package com.example.electronicuniversity.Controller;

import com.example.electronicuniversity.Entity.StudentStatus;
import com.example.electronicuniversity.Entity.dto.StudentStatusDto;
import com.example.electronicuniversity.Service.StudentStatusServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/students/status")
@AllArgsConstructor
public class StudentStatusController {
    private StudentStatusServiceImpl studentStatusService;

    @PreAuthorize("hasAnyRole('TEACHER','RECTOR')")
    @GetMapping
    public List<StudentStatusDto> getStudentStatus(
            @RequestHeader(value = "Authorization") String token
    ) {
        return studentStatusService.getStudentStatuses(token);
    }

    @PreAuthorize("hasAuthority('active')")
    @GetMapping(path = "{studentStatusId}")
    public StudentStatusDto getStudentStatus(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "studentStatusId")Long studentStatusId) {
        return studentStatusService.getStudentStatus(token,studentStatusId);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @PutMapping(path = "{studentStatusId}")
    public void updateStudentStatus(
            @PathVariable(name = "studentStatusId") Long studentStatusId,
            @RequestHeader(value = "Authorization") String token,
            @RequestParam(required = false) Integer grade,
            @RequestParam(required = false) Double attendance) {
        studentStatusService.updateStudentStatus(studentStatusId, token, grade, attendance);
    }

    @PreAuthorize("hasRole('RECTOR')")
    @DeleteMapping(path = "{studentStatusId}")
    public StudentStatus deleteStudentStatus(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "studentStatusId") Long studentStatusId) {
        return studentStatusService.deleteStudentStatus(token,studentStatusId);
    }
}
