package com.example.electronicuniversity.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserPermission {
    STUDENT_READ("student:read"),
    STUDENT_WRITE("student:write"),
    COURSE_READ("course:read"),
    COURSE_WRITE("course:write"),
    TEACHER_READ("teacher:read"),
    TEACHER_WRITE("teacher:write"),
    STUDENT_STATUS_READ("studentStatus:read"),
    STUDENT_STATUS_WRITE("studentStatus:write"),
    ACTIVE("active");

    private String permission;
}
