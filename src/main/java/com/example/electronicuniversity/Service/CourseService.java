package com.example.electronicuniversity.Service;

import com.example.electronicuniversity.Entity.Course;
import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.CourseDto;

import java.util.List;

public interface CourseService {
    List<Course> getCourses(String token);

    Course getCourse(String token,Long courseId);

    Course addCourse(String token,CourseDto courseDto);

    Course deleteCourse(String token,Long courseId);

    String signIn(String token, Long courseId);

    boolean hasTeacher(User teacher);

    Course updateCourse(String token, Long courseId, Long teacherId);
}
