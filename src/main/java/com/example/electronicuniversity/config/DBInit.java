package com.example.electronicuniversity.config;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.UserRole;
import com.example.electronicuniversity.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.example.electronicuniversity.Entity.UserRole.RECTOR;

@Component
public class DBInit {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

//    @PostConstruct
//    public void insertUsers() {
//        User rector = new User(
//                        "admin",
//                        passwordEncoder.encode("password"),
//                        "admin@gmail.com",
//                        RECTOR,
//                        true, true, true, true
//                );
//
//        userRepository.save(rector);
//    }
}
