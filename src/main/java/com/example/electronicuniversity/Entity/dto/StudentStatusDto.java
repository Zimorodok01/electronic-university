package com.example.electronicuniversity.Entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter@Setter
public class StudentStatusDto implements Serializable {
    private Long id;
    private String course;
    private String student;
    private Integer grade;
    private Double attendance;
}
