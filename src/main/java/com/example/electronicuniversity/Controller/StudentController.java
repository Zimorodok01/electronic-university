package com.example.electronicuniversity.Controller;

import com.example.electronicuniversity.Entity.User;
import com.example.electronicuniversity.Entity.dto.UserDto;
import com.example.electronicuniversity.Service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.example.electronicuniversity.Entity.UserRole.STUDENT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@AllArgsConstructor
@RequestMapping(path = "api/v1/students")
public class StudentController {
    @Autowired
    private UserServiceImpl userService;

    @PreAuthorize("hasAuthority('student:read')")
    @GetMapping
    public List<User> getStudents(
            @RequestHeader(value = "Authorization") String token
    ) {
        return userService.getUsers(token, STUDENT);
    }

    @PreAuthorize("hasAuthority('student:read')")
    @GetMapping(path = "{student_id}")
    public User getStudent(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "student_id") Long studentId) {
        return userService.getUser(token, studentId);
    }

    @PreAuthorize("hasRole('RECTOR')")
    @PostMapping
    public User addStudent(
            @RequestHeader(value = "Authorization") String token,
            @RequestBody UserDto userDto) {
        User user = new User(userDto);
        user.setRole(STUDENT);

        return userService.addUser(token, user);
    }

    @PreAuthorize("hasRole('RECTOR')")
    @DeleteMapping(path = "{student_id}")
    public User deleteStudent(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(name = "student_id") Long studentId) {
        User student = userService.getUser(token, studentId);

        if (student.getRole() != STUDENT) {
            throw new ResponseStatusException(BAD_REQUEST,"This id is not studentId");
        }

        return userService.deleteUser(student);
    }
}
